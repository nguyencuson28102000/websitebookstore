<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CategoryProducts;
use App\Models\Products;
use App\Models\Picture;
use App\Http\Requests\ProductRequest;
// use Auth;
use App\Exports\ProductsExport;
use App\Imports\ProductsImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;
use CloudinaryLabs\CloudinaryLaravel\Facades\Cloudinary;


class ProductsController extends Controller
{
    public function AuthLogin()
    {
        $admin_id = Auth::id();
        if ($admin_id) {
            return redirect()->route('/admin/index');
        }else {
            return redirect()->route('login');
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $this->AuthLogin();
        $db = Products::orderBy("id","desc")->paginate(10);
        return view('admin.product.product', compact('db'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $db = CategoryProducts::all();
        // $db = CategoryProducts::where("CategoryName");
        return view("admin.product.add_product", ['db'=>$db]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        //
        if($request->hasfile('fileImg')){
            $file = $request->file('fileImg');
            $obj = Cloudinary::upload($file->getRealPath(), ['folder' => 'products']);
            $public_id = $obj->getPublicId();
            $url = $obj->getSecurePath();
        }
        else {
            return $request;
        }

        $product_id = Products::insertGetId([
            'ProductName' => $request->txtName,
            'Cate_Id' => $request->txtCate,
            'Description' => $request->txtDes,
            'Picture' => $url,
            'Price' => $request->txtprice,
            'Quantity' => $request->nQuantity,
            'Status' => $request->sl_stt,
            'public_id' => $public_id,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        $get_image = $request->file('images');
        if($get_image){
            foreach ($get_image as $image) {
                $uploadedFile = Cloudinary::upload($image->getRealPath(), [
                    'folder' => "products"
                ]);
                $image_public_id = $uploadedFile->getPublicId();
                $uploadedFileUrl = $uploadedFile->getSecurePath();
                if ($product_id) {
                    Picture::insert([
                        'picture' => $uploadedFileUrl,
                        'status' => 1,
                        'ProductId' => $product_id,
                        'public_id' => $image_public_id,
                        'created_at' => now(),
                        'updated_at' => now()
                    ]);
                }

            }
        }
        return redirect()->route('product.index')->with('message', 'Thêm sản phẩm thành công');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $pictures = Products::find($id)->pictures;
        return view("admin.product.show_picture", compact("pictures"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id=null)
    {
        //
        if ($id==null) {
            return redirect()->route("product.index");
        }
        else {
            $db = Products::find($id);
            $categories = CategoryProducts::all();
            $pictures = Products::find($id)->pictures;
            return view("admin.product.edit_product",compact("db","categories","pictures"));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        //
        $db = Products::find($id);
        $db->ProductName = $request->input('txtName');
        $db->Cate_Id = $request->input('txtCate');
        $db->Description = $request->input('txtDes');
        $public_id = $db->public_id;

       if(!$request->hasfile('fileImg')){
          $db->Picture = $request->input("image");
       }
       else {
            if($request->hasfile('fileImg')){
                Cloudinary::destroy($public_id);
                $file = $request->file('fileImg');
                $obj = Cloudinary::upload($file->getRealPath(), ['folder' => 'products']);
                $public_id = $obj->getPublicId();
                $url = $obj->getSecurePath();
                $db->Picture = $url;
                $db->public_id = $public_id;
            }
            else {
                $db->Picture = "";
                $db->public_id = "";
            }
       }


       $get_image = $request->file('images');
       if($get_image){
            $pictures = Picture::where("ProductId", $id)->get();
            foreach ($pictures as $picture) {
                Cloudinary::destroy($picture->public_id);
            }
            Picture::where("ProductId", $id)->delete();
            foreach ($get_image as $image) {
               $objImageDetail = Cloudinary::upload($image->getRealPath(), ['folder' => 'products']);
               $image_public_id = $objImageDetail->getPublicId();
               $image_url = $objImageDetail->getSecurePath();
               if ($id) {
                    Picture::insert([
                       'picture' => $image_url,
                       'status' => 1,
                       'ProductId' => $id,
                       "public_id" => $image_public_id,
                       'created_at' => now(),
                       'updated_at' => now()
                   ]);
               }
           }
        }
        $db->Price = $request->input('txtprice');
        $db->Quantity = $request->input('nQuantity');
        $db->Status = $request->input('sl_stt');
        $db->save();
        return redirect()->route("product.index", [$id])->with('message', 'Cập nhật sản phẩm thành công');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $db = Products::findOrFail($id);
        Cloudinary::destroy($db->public_id);
        $pictures = Picture::where("ProductId", $id)->get();
        foreach ($pictures as $picture) {
            Cloudinary::destroy($picture->public_id);
        }
        $db->delete();
        return redirect()->route("product.index")->with('message', 'Xóa sản phẩm thành công');
    }


    public function search(Request $request)
    {
        //
        $text = $request->input("txtSearch");
        if ($text == "") {
            $db = Products::paginate(10);
        }
        else {
            $db = Products::join('category_products','products.Cate_id','=','category_products.id')
                           ->select('products.*')
                           ->where('products.ProductName','LIKE','%'.$text.'%')
                           ->orWhere('products.id','LIKE','%'.$text.'%')
                           ->orWhere('products.Price','LIKE','%'.$text.'%')
                           ->orWhere('category_products.CategoryName','LIKE','%'.$text.'%')->paginate(1000);
        }
        return view('admin.product.product', ['db'=>$db]);
    }


    /**
    * @return \Illuminate\Support\Collection
    */
    public function export()
    {
        return Excel::download(new ProductsExport, 'products.xlsx');
    }


    /**
    * @return \Illuminate\Support\Collection
    */
    public function import()
    {
        Excel::import(new ProductsImport,request()->file('file'));
        return back();
    }
}
